USE tussentijdseevaluatie1bdb;

SELECT concat(leden.Voornaam," ",leden.Familienaam) AS Naam, boeken.Titel

FROM leden

INNER JOIN boeken ON leden.Id = boeken.Id

