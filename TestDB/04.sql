USE tussentijdseevaluatie1bdb;

CREATE VIEW BoekenInfo

AS

SELECT boeken.Titel, concat(auteurs.Voornaam," ",auteurs.Familienaam) AS NaamAuteur, uitgeverijen.Naam

FROM boeken
INNER JOIN
(auteurs INNER JOIN uitgeverijen ON auteurs.Id = uitgeverijen.Id)

ON auteurs.Id = boeken.Id