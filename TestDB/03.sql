USE tussentijdseevaluatie1bdb;

SELECT boeken.Titel, uitgeverijen.Naam

FROM boeken

INNER JOIN uitgeverijen ON boeken.Id = uitgeverijen.Id

WHERE uitgeverijen.Naam = "Papyrus"

