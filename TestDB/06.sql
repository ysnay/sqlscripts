USE tussentijdseevaluatie1bdb;

SELECT

COALESCE (concat(leden.Voornaam," ",leden.Familienaam), 'is nog nooit door iemand uitgeleend') AS Naam,
COALESCE (boeken.Titel, 'heeft nog nooit iets uitgeleend')  AS Titel

FROM leden

RIGHT JOIN boeken ON leden.Id = boeken.Id 

ORDER BY leden.Familienaam, leden.Voornaam, boeken.Titel;
