CREATE DATABASE IF NOT EXISTS Test;

USE Test;

CREATE TABLE Personen(
Id INT auto_increment PRIMARY KEY,
Voornaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Achternaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Adres VARCHAR(100) CHAR SET utf8mb4
);

CREATE TABLE Boeken(
Id INT PRIMARY KEY auto_increment,
Personen_Id INT,
Titel VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
constraint fk_Boeken_Personen FOREIGN KEY (Personen_Id)
REFERENCES Personen(Id)
);




