USE ModernWays;
ALTER VIEW auteursboeken
AS
select concat(personen.Voornaam,' ', personen.familienaam) as "Auteur",
        titel,
        boeken.Id as 'Boeken_Id'
from boeken inner join publicaties
on boeken.id = Publicaties.Boeken_Id

inner join personen
on personen.id = Publicaties.Personen_Id;

