USE Modernways;
CREATE TABLE Uitleningen(
    Leden_Id int NOT NULL,
    Boeken_Id int NOT NULL,
    Startdatum DATE NOT NULL,
    Einddatum DATE,
    CONSTRAINT fk_Uitleningen_Leden FOREIGN KEY (Leden_Id) REFERENCES Leden (Id),
    CONSTRAINT fk_Uitleningen_Boeken FOREIGN KEY (Boeken_Id) REFERENCES Boeken (Id)
);